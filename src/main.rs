#![deny(clippy::all, unsafe_code)]

use std::{
    fs::{self},
    io::{prelude::*, BufReader},
    os::unix::net::UnixStream,
    process::Command,
};

use camino::Utf8PathBuf;

fn main() -> std::io::Result<()> {
    let handlers = Handlers::new();
    eprintln!("{:?}", &handlers);

    let stream = UnixStream::connect("/var/run/acpid.socket")?;
    stream.set_read_timeout(None)?;

    match read_lid_state() {
        AcpiEvent::ButtonLidClose => execute(&handlers.button_lid_close),
        AcpiEvent::ButtonLidOpen => execute(&handlers.button_lid_open),
        _ => { /* noop */ }
    }

    let reader = BufReader::new(stream);
    for line in reader.lines().flatten() {
        match AcpiEvent::from(line) {
            AcpiEvent::ButtonLidClose => execute(&handlers.button_lid_close),
            AcpiEvent::ButtonLidOpen => execute(&handlers.button_lid_open),
            _ => { /* noop */ }
        }
    }

    Ok(())
}

const BUTTON_LID_CLOSE: &str = "button/lid LID close";
const BUTTON_LID_OPEN: &str = "button/lid LID open";

#[derive(Debug, PartialEq)]
enum AcpiEvent {
    ButtonLidClose,
    ButtonLidOpen,
    Other(String),
}
impl From<&str> for AcpiEvent {
    fn from(source: &str) -> Self {
        match source {
            BUTTON_LID_CLOSE => Self::ButtonLidClose,
            BUTTON_LID_OPEN => Self::ButtonLidOpen,
            _ => Self::Other(String::from(source)),
        }
    }
}
impl From<String> for AcpiEvent {
    fn from(source: String) -> Self {
        AcpiEvent::from(source.as_str())
    }
}

#[derive(Debug)]
struct Handlers {
    button_lid_close: Option<Utf8PathBuf>,
    button_lid_open: Option<Utf8PathBuf>,
}
impl Handlers {
    fn new() -> Self {
        Self {
            button_lid_close: find_config_file(&"button-lid-close"),
            button_lid_open: find_config_file(&"button-lid-open"),
        }
    }
}

// yep, there is a TOCTOU race here, but we're just trying to be helpful to the user
fn find_config_file(name: &dyn AsRef<str>) -> Option<Utf8PathBuf> {
    // TODO: check that file is also executable
    match dirs::config_dir() {
        Some(config_dir) => {
            let exe_path = config_dir.join(env!("CARGO_PKG_NAME")).join(name.as_ref());
            if exe_path.exists() {
                if let Ok(pb) = Utf8PathBuf::from_path_buf(exe_path) {
                    return Some(pb);
                }
            }
            None
        }
        None => None,
    }
}

fn find_lid_state_files() -> Vec<Utf8PathBuf> {
    let proc_acpi_button_lid_dir = Utf8PathBuf::from("/proc/acpi/button/lid");
    if !proc_acpi_button_lid_dir.is_dir() {
        return Vec::new();
    }
    match proc_acpi_button_lid_dir.read_dir() {
        Ok(read_dir) => read_dir
            .flatten()
            .filter(|lid_dir| lid_dir.path().is_dir())
            .map(|lid_dir| lid_dir.path().join("state"))
            .filter(|state_file| state_file.is_file())
            .map(Utf8PathBuf::from_path_buf)
            .flatten()
            .collect(),
        Err(_err) => Vec::new(),
    }
}

fn execute(maybe_exe_path: &Option<Utf8PathBuf>) {
    if let Some(exe_path) = maybe_exe_path {
        match Command::new(&exe_path).status() {
            Ok(status) => match status.code() {
                Some(code) => eprintln!("{} -> exit status {}", &exe_path, code,),
                None => eprintln!("{} -> terminated by signal", &exe_path),
            },
            Err(err) => eprintln!("could not execute {}: {:?}", &exe_path, err),
        }
    }
}

fn read_lid_state() -> AcpiEvent {
    for state_file in find_lid_state_files() {
        if let Ok(data) = fs::read_to_string(state_file) {
            if data.contains("closed") {
                return AcpiEvent::ButtonLidClose;
            }
        }
    }
    AcpiEvent::ButtonLidOpen
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn acpievent_buttonlidclose_from_string() {
        let got = AcpiEvent::from("button/lid LID close");
        assert_eq!(got, AcpiEvent::ButtonLidClose);
    }

    #[test]
    fn acpievent_buttonlidopen_from_string() {
        let got = AcpiEvent::from("button/lid LID open");
        assert_eq!(got, AcpiEvent::ButtonLidOpen);
    }

    #[test]
    fn acpievent_other_from_string() {
        let got = AcpiEvent::from("something else");
        assert_eq!(got, AcpiEvent::Other(String::from("something else")));
    }
    #[test]
    fn find_config_file_bad_path_does_not_panic() {
        find_config_file(&"foo");
    }

    #[test]
    fn find_lid_state_files_does_not_panic() {
        find_lid_state_files();
    }

    #[test]
    fn execute_bad_path_does_not_panic() {
        execute(&Some(Utf8PathBuf::from("something-that-does-not-exist")));
    }

    #[test]
    fn read_lid_state_does_not_panic() {
        read_lid_state();
    }
}
